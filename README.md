# ssh-client-alpine

SSH Client Based on Alpine Linux for CI/CD

## how to build
docker build --no-cache=true --build-arg BUILD_DATE=$(date -u +'%Y-%m-%dT%H:%M:%SZ') -t innovatetech/ssh-client-alpine .

## how to push after loging in to docker hub
docker push innovatetech/ssh-client-alpine