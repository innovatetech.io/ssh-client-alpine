FROM alpine:3.6

LABEL maintainer="Buddhi Raj Tuladhar <tuladhar.raj@gmail.com>"

ARG VCS_REF
ARG BUILD_DATE

# Metadata
LABEL org.label-schema.vcs-ref=$VCS_REF \
      org.label-schema.vcs-url="https://gitlab.com/innovatetech.io/ssh-client-alpine" \
      org.label-schema.build-date=$BUILD_DATE \
      org.label-schema.docker.dockerfile="/Dockerfile"

RUN apk update && apk add openssh-client